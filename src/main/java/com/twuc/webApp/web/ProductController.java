package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import com.twuc.webApp.exception.ProjectIdNotExistException;
import com.twuc.webApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
public class ProductController {

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/api/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest productRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("location", "http://localhost/api/products/" + productService.createProduct(productRequest))
                .build();
    }

    @GetMapping("/api/products/{productId}")
    public ResponseEntity getProduct(@PathVariable Long productId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(productService.getProduct(productId));
    }

    @ExceptionHandler(ProjectIdNotExistException.class)
    public ResponseEntity handleProjectIdNotExistException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .build();
    }
}
