package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import com.twuc.webApp.exception.ProjectIdNotExistException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Long createProduct(CreateProductRequest productRequest) {

//        Product product = new Product();
//        BeanUtils.copyProperties(productRequest, product);
        Product product = new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit());
        Product savedProduct = productRepository.save(product);
        productRepository.flush();
        return savedProduct.getId();
    }

    @Override
    public GetProductResponse getProduct(Long productId) {
        return new GetProductResponse(productRepository.findById(productId).orElseThrow(ProjectIdNotExistException::new));
    }
}
