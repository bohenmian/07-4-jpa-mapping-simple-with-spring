package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;

public interface ProductService {

    Long createProduct(CreateProductRequest product);

    GetProductResponse getProduct(Long productId);


}
